<?php

/**
 * Copyright (c) 2015 Bernardo van der Wal
 * MIT License
 *
 * Refer to the LICENSE file for the full copyright notice.
 */

namespace OneOfZero\SwissArmyKnife\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\AnnotationReader;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

class Annotations
{
	/**
	 * @var AnnotationReader $annotationReader
	 */
	private $annotationReader;

	/**
	 * @param AnnotationReader $annotationReader
	 */
	public function __construct(AnnotationReader $annotationReader)
	{
		$this->annotationReader = $annotationReader;
	}

	/**
	 * @param ReflectionClass|ReflectionMethod|ReflectionProperty|object|array|string $source
	 * @param string|null $annotationName
	 * @return Annotation
	 * @throws InvalidArgumentException
	 */
	public function get($source, $annotationName = null)
	{
		// TODO: Add container support?

		if (is_string($source) && class_exists($source))
		{
			$source = new ReflectionClass($source);
		}

		if (is_array($source))
		{
			$class = is_object($source[0]) ? get_class($source[0]) : $source[0];

			if (class_exists($class) && method_exists($source[0], $source[1]))
			{
				$source = new ReflectionMethod($source[0], $source[1]);
			}
			elseif (class_exists($class) && property_exists($source[0], $source[1]))
			{
				$source = new ReflectionProperty($source[0], $source[1]);
			}
		}

		if (is_object($source))
		{
			if ($source instanceof ReflectionClass)
			{
				return $annotationName
					? $this->annotationReader->getClassAnnotation($source, $annotationName)
					: $this->annotationReader->getClassAnnotations($source)
				;
			}

			if ($source instanceof ReflectionMethod)
			{
				return $annotationName
					? $this->annotationReader->getMethodAnnotation($source, $annotationName)
					: $this->annotationReader->getMethodAnnotations($source)
				;
			}

			if ($source instanceof ReflectionProperty)
			{
				return $annotationName
					? $this->annotationReader->getPropertyAnnotation($source, $annotationName)
					: $this->annotationReader->getPropertyAnnotations($source)
				;
			}

			return $annotationName
				? $this->annotationReader->getClassAnnotation(new ReflectionClass($source), $annotationName)
				: $this->annotationReader->getClassAnnotations(new ReflectionClass($source))
			;
		}

		throw new InvalidArgumentException('Could not resolve the reference provided as $source');
	}

	/**
	 * @param ReflectionClass|ReflectionMethod|ReflectionProperty|object|array|string $source
	 * @param string|null $annotationName
	 * @return bool
	 * @throws InvalidArgumentException
	 */
	public function has($source, $annotationName = null)
	{
		return $this->get($source, $annotationName) !== null;
	}
}

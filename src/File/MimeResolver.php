<?php

namespace OneOfZero\SwissArmyKnife\File;

use OneOfZero\SwissArmyKnife\File\MimeTypeProviders\ProviderInterface;
use OneOfZero\SwissArmyKnife\File\MimeTypeProviders\SystemMimeProvider;
use ReflectionClass;

class MimeResolver
{
	const BUILTIN_RESOLVERS = [
		SystemMimeProvider::class
	];

	/**
	 * @var array $extensionMap
	 */
	private $extensionMap = [];

	/**
	 * @var array $mimeMap
	 */
	private $mimeMap = [];

	/**
	 *
	 */
	public function __construct()
	{
		foreach (self::BUILTIN_RESOLVERS as $resolverClass)
		{
			if (!class_exists($resolverClass))
			{
				continue;
			}

			$resolverClass = new ReflectionClass($resolverClass);
			if (!$resolverClass->implementsInterface(ProviderInterface::class))
			{
				continue;
			}

			/** @var ProviderInterface $instance */
			$instance = $resolverClass->newInstance();
			$instance->load($this);
		}
	}

	/**
	 * @param string $extension
	 * @param string $mimeType
	 */
	public function register($extension, $mimeType)
	{
		if (!array_key_exists($extension, $this->extensionMap))
		{
			$this->extensionMap[$extension] = [ $mimeType ];
		}
		elseif (!in_array($mimeType, $this->extensionMap[$extension]))
		{
			$this->extensionMap[$extension][] = $mimeType;
		}

		if (!array_key_exists($mimeType, $this->mimeMap))
		{
			$this->mimeMap[$mimeType] = [ $extension ];
		}
		elseif (!in_array($extension, $this->mimeMap[$mimeType]))
		{
			$this->mimeMap[$mimeType][] = $extension;
		}
	}

	/**
	 * @param string $path
	 * @return bool|string
	 */
	public function resolveFileMimeType($path)
	{
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		return $this->resolveMimeType($extension);
	}

	/**
	 * @param string $extension
	 * @return string|bool
	 */
	public function resolveMimeType($extension)
	{
		$mimeTypes = $this->resolveMimeTypes($extension);
		return $mimeTypes ? $mimeTypes[0] : false;
	}

	/**
	 * @param string $extension
	 * @return string[]
	 */
	public function resolveMimeTypes($extension)
	{
		if (!array_key_exists($extension, $this->extensionMap))
		{
			return [];
		}
		return $this->extensionMap[$extension];
	}

	/**
	 * @param string $mimeType
	 * @return string|bool
	 */
	public function resolveExtension($mimeType)
	{
		$extensions = $this->resolveExtensions($mimeType);
		return $extensions ? $extensions[0] : false;
	}

	/**
	 * @param string $mimeType
	 * @return string[]
	 */
	public function resolveExtensions($mimeType)
	{
		if (!array_key_exists($mimeType, $this->mimeMap))
		{
			return [];
		}
		return $this->mimeMap[$mimeType];
	}
}

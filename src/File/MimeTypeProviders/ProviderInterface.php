<?php

namespace OneOfZero\SwissArmyKnife\File\MimeTypeProviders;

use OneOfZero\SwissArmyKnife\File\MimeResolver;

interface ProviderInterface
{
	public function load(MimeResolver $resolver);
}

<?php

namespace OneOfZero\SwissArmyKnife\File\MimeTypeProviders;

use OneOfZero\SwissArmyKnife\File\MimeResolver;

class SystemMimeProvider implements ProviderInterface
{
	const MIME_FILE = '/etc/mime.types';

	/**
	 * @param MimeResolver $resolver
	 */
	public function load(MimeResolver $resolver)
	{
		if (!is_readable(self::MIME_FILE))
		{
			return;
		}

		$lines = file(self::MIME_FILE);
		foreach ($lines as $line)
		{
			if (!trim($line) || trim($line)[0] === '#')
			{
				continue;
			}

			$matches = preg_split('/\s+/', $line, -1, PREG_SPLIT_NO_EMPTY);
			$mimeType = array_shift($matches);
			if ($matches)
			{
				foreach ($matches as $extension)
				{
					$resolver->register($extension, $mimeType);
				}
			}
		}
	}
}

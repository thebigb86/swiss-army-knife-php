<?php

namespace OneOfZero\SwissArmyKnife\Tests;

use OneOfZero\SwissArmyKnife\File\MimeResolver;
use PHPUnit_Framework_TestCase;

class MimeResolverTest extends PHPUnit_Framework_TestCase
{
	public function testEverything()
	{
		$mimeResolver = new MimeResolver();
		$this->assertEquals('json', $mimeResolver->resolveExtension('application/json'));
		$this->assertEquals('application/json', $mimeResolver->resolveMimeType('json'));
		$this->assertEquals('application/json', $mimeResolver->resolveFileMimeType('file.json'));
	}
}
